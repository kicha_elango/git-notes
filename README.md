# Git Version Control System

Overview of VCS concepts, terminologies, commands and best practices.

## Topics

1. Introduction
2. Repositories: Local and Remote

## Commands

|Command|Description|
|-----|-----|
|`init`|create a local repository|
|`clone`|copy a remote repository to local |
|`add`|stage changed files for future commits |
|`status`|check the status of files; staged?, added?, modified?? etc..|
|`commit`|create snapshot|
|`push`|sync changes from local to remote |
